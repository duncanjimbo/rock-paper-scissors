(function($, window, document) {
	var wins = 0;
	var losses = 0;
	var ties = 0;
	var timerId = 0;
	
	function isInteger(n) {
		return n % 1 === 0;
	}

	function startGame(min, sec) {
		$('.user-play-options button').removeAttr('disabled');
		countDown(min, sec);
	}
	
	function zeroPad(num, places) {
		var zero = places - num.toString().length + 1;
		return new Array(+(zero > 0 && zero)).join("0") + num;
	}

	function countDown(min, sec) {
		var $minutesDisplay = $('.timer .minutes');
		var $secondsDisplay = $('.timer .seconds');
		var $timerText = $('.set-timer p');
		var minDisp, secDisp;
		
		timerId = setInterval(function(){
			if (sec === 0) {
				if (min === 0) {
					clearInterval(timerId);
					endGame();
				}
				else {
					min--;
					minDisp = zeroPad(min, 2);
					$minutesDisplay.text(minDisp);
					
					secDisp = sec = 59;
					$secondsDisplay.text(secDisp);
				}
			}
			else {
				sec--;
				secDisp = zeroPad(sec, 2);
				$secondsDisplay.text(secDisp);
				
				if (min === 0 && sec === 10) {
					$timerText.text('Ten seconds left!');
		
					if ( $timerText.is(':hidden')) {
						$timerText.slideDown('fast');
					}
				}
				if (min === 0 && sec === 7) {
					$timerText.slideUp('fast');
				}
			}
		},1000);
	}
	
	function endGame() {
		$('.user-play-options button').attr('disabled', 'disabled');
		$('.user-selection, .bot-selection').removeClass('is-winner').text('...');
		var $gameResult = $('.game-result');
		
		if (wins > losses) {
			$gameResult.text('Congratulations! You beat the bot!');
		}
		else if (losses > wins) {
			$gameResult.text('Sorry. The bot beat you.');
		}
		else {
			$gameResult.text('The game was tied.');
		}
		$gameResult.slideDown('fast');
	}
	
	function validateInputs(){
		var minutes = $('input[name="minutes"]').val();
		var seconds = $('input[name="seconds"]').val();
		var $timerText = $('.set-timer p');
		var minDisp, secDisp;
		
		// Set defaults and convert to numbers
		if ( minutes === '' ) {
			minutes = 0;
		}
		else {
			minutes = +minutes;
		}
	
		if ( seconds === '' ) {
			seconds = 0;
		}
		else {
			seconds = +seconds;
		}
	
		// Check that these are numbers and integers at that
		if (( isNaN(minutes) || isNaN(seconds) ) || ( minutes === 0 && seconds === 0 )) {
			$timerText.text('Uh oh! Looks like something is wrong with the values you entered');
		
			if ( $timerText.is(':hidden')) {
				$timerText.slideDown('3000');
			}
		}
		else if ( minutes > 59 || seconds > 59 ) {
			$timerText.text('Please enter minutes between 0 and 59 and seconds between 0 and 59 to simulate a real timer');
		}
		else {
			if ( isInteger(minutes) && isInteger(seconds) ) {
				$('.set-timer p').slideUp('3000');
				
				minDisp = zeroPad(minutes, 2);
				$('.timer .minutes').text(minDisp);
				
				secDisp = zeroPad(seconds, 2);
				$('.timer .seconds').text(secDisp);
				
				startGame(minutes, seconds);
			}
		}
	}

	function playBot() {
		var botOptions = ['Rock','Paper','Scissors'];
		var botSelection = botOptions[Math.floor(Math.random() * botOptions.length)];
		return botSelection;
	}

	function gameWinner(human, bot) {
		if ((human === 'Rock' && bot === 'Scissors') || (human === 'Paper' && bot === 'Rock') || (human === 'Scissors' && bot === 'Paper')) {
			return 'human';
		}
		else if ((bot === 'Rock' && human === 'Scissors') || (bot === 'Paper' && human === 'Rock') || (bot === 'Scissors' && human === 'Paper')) {
			return 'bot';
		}
		else {
			return 'tie';
		}
	}

	function updateScore(result) {
		switch(result) {
			case 'win':
				wins += 1;
				$('.score-board .wins span').text( wins );
			break;
		
			case 'loss':
				losses += 1;
				$('.score-board .losses span').text( losses );
			break;
		
			case 'tie':
				ties += 1;
				$('.score-board .ties span').text( ties );
			break;
		}
	}
	
	function resetBoard() {
		wins = 0;
		losses = 0;
		ties = 0;
		
		clearInterval(timerId);
		$('.score-board .wins span').text( wins );
		$('.score-board .losses span').text( losses );
		$('.score-board .ties span').text( ties );
		$('.user-selection, .bot-selection').removeClass('is-winner').text('...');
		$('.game-result').slideUp('fast', function(){
			$(this).text('');
		});
		setTimeout(function(){
			validateInputs();
		}, 500);
	}


	$('.button-restart').on('click', function(e){
		e.preventDefault();
		resetBoard();
	});
	
	$('.user-play-options button').on('click', function(e){
		e.preventDefault();
		$('.user-selection, .bot-selection').removeClass('is-winner');
		
		var human = $(this).text();
		var bot = playBot();
		var winner = gameWinner(human, bot);
		
		if (winner === 'human') {
			$('.user-selection').addClass('is-winner');
			updateScore('win');
		}
		else if (winner === 'bot') {
			$('.bot-selection').addClass('is-winner');
			updateScore('loss');
		}
		else {
			updateScore('tie');
		}
		
		$('.user-selection').text(human);
		$('.bot-selection').text(bot);
	});
}(window.jQuery, window, document));




